import java.util.ArrayList;

public class Main {
    public static void main(String[] args)
    {
        // Example
        /*Node node = new Node(4);
        node.appendToEnd(5);
        node.appendToEnd(6);
        node.appendToEnd(7); */
        // End of Example

        // first Customer 5 unit away from factory
        // Dealer have to come back to factory after every visit. So, multiply the paths with 2
        Node shortestWayLinkedList=new Node(5*2);
        shortestWayLinkedList.appendToEnd(7*2);
        shortestWayLinkedList.appendToEnd(5*2);
        shortestWayLinkedList.appendToEnd(5*2);
        shortestWayLinkedList.appendToEnd(8*2);
        shortestWayLinkedList.appendToEnd(7*2);
        shortestWayLinkedList.appendToEnd(11*2);
        shortestWayLinkedList.appendToEnd(7*2); // dealer is going back to factory after customer visit over

        int totalDistance = shortestWayLinkedList.sumOfNodes();
        int length = shortestWayLinkedList.length(shortestWayLinkedList);
        System.out.println("Dealaer have " + length + " customers to visit");
        System.out.println("Dealaer have to go " + totalDistance + " units in total to be able to visit all customers..\n");
        shortestWayLinkedList.printNodes();

        while(shortestWayLinkedList!=null)
        {
            shortestWayLinkedList=shortestWayLinkedList.deleteNode(shortestWayLinkedList,shortestWayLinkedList.data);
        }

        if(shortestWayLinkedList==null)
            System.out.println( "No nodes found!!!");



        /*node.printNodes();
        System.out.println(node.length(node));
        node.deleteNode(node,7);
        node.printNodes();
        System.out.println(node.length(node));
        System.out.println(node.sumOfNodes()); */
    }
}
