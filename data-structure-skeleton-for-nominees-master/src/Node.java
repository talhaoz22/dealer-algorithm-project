public class Node
{
    Node following = null;

    int data;

    public Node(int data) {
        this.data = data;
    }

    public void appendToEnd(int data) {
        Node end = new Node(data);
        Node n = this;

        while (n.following != null) {
            n = n.following;
        }
        n.following = end;
    }

    // IMPLEMENTED ******************
    // For example:: --> 5 --> 6 --> 7 --> 3 --> .
    public void printNodes()
    {
        Node currNode = this;

        while(currNode != null)
        {
            System.out.print(currNode.data);
            if(currNode.following!=null)
                System.out.print(" --> "); // for good looking

            currNode=currNode.following;
        }
        System.out.println();

    }

    // IMPLEMENTED ******************
    int length(Node h)
    {
        int count = 0;
        Node currNode=h;
        while(currNode!=null)
        {
            count++;
            currNode=currNode.following;
        }
        return count;
    }

    // IMPLEMENTED ******************
    int sumOfNodes()
    {
        int sum = 0;
        Node currNode=this;
        while(currNode!=null)
        {
            sum +=currNode.data;
            currNode=currNode.following;
        }

        return sum;
    }

    // IMPLEMENTED ******************
    Node deleteNode(Node head, int data) {



        Boolean flagForDelete=false;
        Node n = head;
        if (n.data == data) {
            return head.following;
        }

        Node iterNode = head.following; // because the data in the Head Node already checked
        Node prevNode=head;
        while(iterNode!=null)
        {
            if(iterNode.data == data) { // if this is data that we are looking for then relink the prev and following nodes and delete the node ,
                prevNode.following = iterNode.following;
                iterNode=null;
                flagForDelete=true;
            }
            else {
                prevNode=iterNode;
                iterNode = iterNode.following;
            }

        }

        if(flagForDelete)
            return head;
        else
            return null; // if data is not available in this linked list return null
    }
}